import {Component, OnInit} from '@angular/core';
import {AuthService} from '../service/auth.service';

@Component({
    selector: 'app-registration-page',
    templateUrl: './registration-page.component.html',
    styleUrls: ['./registration-page.component.scss'],
})
export class RegistrationPageComponent implements OnInit {
    public error = '';
    public email = '';
    public password = '';
    public password_confirmation = '';
    public name = '';

    constructor(
        private auth: AuthService
    ) {
    }

    ngOnInit() {
    }

    doRegistartion() {
        this.error = '';

        if (this.email === '' && this.name === '' && this.password_confirmation === '' && this.password === '') {
            this.error = 'Проверьте правильность заполнения полей';
            return;
        }

        if (this.password !== this.password_confirmation) {
            this.error = 'Пароли должны совпадать';
            return;
        }

        this.auth.registration(this.email, this.password, this.name)
            .subscribe(res => {
                this.auth.login(this.email, this.password);
            }, err => {
                this.error = err.error.message;
                console.error(err);
            });
    }
}
