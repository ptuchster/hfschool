import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';
import {AuthService} from './service/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    private RESTRICTED_PATHS = [
        '/login',
        '/registration'
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
        private auth: AuthService
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.router.events.subscribe((res: any) => {
                if (res.constructor.name !== 'RoutesRecognized') {
                    return;
                }

                // Переадресация на страницу логина когда мы пользователя не знаем
                if (!this.auth.loggedIn() && !this.RESTRICTED_PATHS.includes(res.url)) {
                    this.router.navigateByUrl('/login');
                }

                // Переадресация на главную когда мы пользователя знаем
                if (this.auth.loggedIn() && this.RESTRICTED_PATHS.includes(res.url)) {
                    this.router.navigateByUrl('');
                }
                // console.log(res);
                    }, err => {
                console.error(err);
            });
        });
    }
}
