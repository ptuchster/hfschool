import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {share} from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    constructor(
        private auth: AuthService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.auth.loggedIn()) {
            // Инжектим заголовок с токеном
            console.log(this.auth.currentUser());
            const token = this.auth.currentUser()['user-token'];

            let newReq = req;

            newReq = newReq.clone({
                headers: newReq.headers.set('user-token', token)
            });
            console.log(newReq);

            const seq = next.handle(newReq).pipe(share());

            seq.subscribe(res => {

            }, err => {

            });

            return seq;
        } else {
            return next.handle(req);
        }
    }
}
