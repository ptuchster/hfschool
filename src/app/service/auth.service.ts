import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {share} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private host = 'https://api.backendless.com/';
    private appID = 'ADAEFF74-3B0A-CA21-FFB9-0DBA2614AA00';
    private restID = 'ED822E8E-E75C-4C2A-BBDF-A5F845165B84';

    constructor(
        private httpClient: HttpClient,
        private router: Router
    ) {
    }

    public buildAddr() {
        return this.host + this.appID + '/' + this.restID;
    }

    private saveUser(user: object) {
        window.localStorage.setItem('user', JSON.stringify(user));
    }

    private destroyUser() {
        window.localStorage.removeItem('user');
    }

    public currentUser(): any {
        // TODO проверка валидности токена на бакенде
        return JSON.parse(window.localStorage.getItem('user'));
    }

    public loggedIn(): boolean {
        return this.currentUser() != null;
    }

    public login(email: string, password: string): Observable<any> {
        const seq = this.httpClient.post(this.host + this.appID + '/' + this.restID + '/users/login', {
            login: email,
            password: password
        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).pipe(share());

        seq.subscribe(res => {
            console.log(res);
            this.saveUser(res);
            this.router.navigateByUrl('');
        }, err => {
            console.error(err);
        });

        return seq;
    }

    public logout() {
        this.destroyUser();
        this.router.navigateByUrl('/login');
    }

    public registration(email: string, password: string, name: string) {
        const seq = this.httpClient.post(this.host + this.appID + '/' + this.restID + '/users/register', {
            email: email,
            password: password,
            name: name
        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).pipe(share());

        seq.subscribe(res => {
            console.log('======================', res);
            // this.saveUser(res);
        }, err => {
            console.error(err);
        });

        return seq;
    }

    log(target: object, log: string, object: any) {
        console.log(target.constructor.name + ' >>> ' + log, object);
    }
}
