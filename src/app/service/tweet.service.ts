import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TweetService {

    constructor(
        private http: HttpClient,
        private auth: AuthService
    ) {
    }

    createTweet(tweet_text: string): Observable<TweetInterface> {
        const host = this.auth.buildAddr();
        const user = this.auth.currentUser();

        return new Observable(subscriber => {
            const seq = this.http.post(host + '/data/tweets', {
                text: tweet_text
            }).subscribe((res: TweetInterface) => {
                this.auth.log(this, 'created', res);
                const current_user = this.auth.currentUser();

                const seqR = this.http.post(host + '/data/tweets/' + res.objectId + '/user', [
                    current_user.objectId
                ]).subscribe(resR => {
                    subscriber.next(res);
                }, err => {
                    console.log(err);
                    subscriber.error(err);
                });
            }, err => {
                this.auth.log(this, 'Ошибка создания объекта', err);
                subscriber.error(err);
            });
        });
    }
}

export interface TweetInterface {
    objectId: string;
    text: string;
    user: object | null;
}
