import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../service/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
    public email: string;
    public password: string;
    public error = '';

    constructor(
        private authService: AuthService,
        private router: Router
    ) {
    }

    ngOnInit() {
    }

    doLogin() {
        this.error = '';
        if (this.email !== '' && this.password !== '') {
            this.authService.login(this.email, this.password)
                .subscribe(res => {
                    // Пользователь залогинился
                }, err => {
                    // Произошла какая-то ошибка
                    if (err.error.code === 3003) {
                        this.error = 'Неверный логин или пароль';
                    }
                    if (err.error.code === 3006) {
                        this.error = 'Пароль не может быть пустым';
                    }
                });
        }
    }

    navigateToRegister() {
        this.router.navigateByUrl('/registration');
    }
}
