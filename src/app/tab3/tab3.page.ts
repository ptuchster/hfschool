import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  constructor(
      private httpClient: HttpClient,
      private auth: AuthService
  ) {
    this.preloadData();
  }

  preloadData() {
    this.httpClient.get('https://api.backendless.com/ADAEFF74-3B0A-CA21-FFB9-0DBA2614AA00/ED822E8E-E75C-4C2A-BBDF-A5F845165B84/data/todo')
        .subscribe(res => {
          console.log(res);
        }, err => {
          console.error(err);
        });
  }

  doLogout() {
      this.auth.logout();
  }
}
