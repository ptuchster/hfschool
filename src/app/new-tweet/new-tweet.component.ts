import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TweetService} from '../service/tweet.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
    selector: 'app-new-tweet',
    templateUrl: './new-tweet.component.html',
    styleUrls: ['./new-tweet.component.scss'],
})
export class NewTweetComponent implements OnInit {
    public counter = 0;
    public form: FormGroup;
    public createError: string;

    // TODO директива на девалидацию длинны
    constructor(
        private tService: TweetService,
        private fb: FormBuilder,
        private router: Router
    ) {
        this.form = fb.group({
            tweet_text: [
                '',
                Validators.compose([
                    Validators.maxLength(255),
                    Validators.minLength(1),
                    Validators.required
                ])
            ]
        });
    }

    ngOnInit() {
    }

    changeCounter(event: string) {
        this.counter = event.length;
    }

    tweet() {
        if (this.form.valid) {
            this.tService
                .createTweet(this.form.value.tweet_text)
                .subscribe(res => {
                    this.form.controls.tweet_text.setValue('');
                    // TODO
                    this.router.navigateByUrl('/');
                }, (err: HttpErrorResponse) => {
                    this.createError = err.message;
                    console.log(err);
                });
        }
    }

}
